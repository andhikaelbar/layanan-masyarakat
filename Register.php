<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="container">
	<div class="form-register">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card border-0 shadow rounded-3 my-5">
          <div class="card-body p-4 p-sm-5">
            <h5 class="card-title text-center mb-5 fw-light fs-5">Register</h5>
            <form method="POST" action="Daftar.php">
              <div class="form-floating mb-3">
                <input name="nik" type="number" class="form-control" id="floatingInput" placeholder="nik">
                <label for="floatingInput">Nik</label>
              </div>
              <div class="form-floating mb-3">
                <input name="nama" type="text" class="form-control" id="floatingInput" placeholder="nama">
                <label for="floatingInput">Nama</label>
              </div>
              <div class="form-floating mb-3">
                <input name="username" type="text" class="form-control" id="floatingInput" placeholder="username">
                <label for="floatingInput">Username</label>
              </div>
              <div class="form-floating mb-3">
                <input name="password" type="password" class="form-control" id="floatingPassword" placeholder="password">
                <label for="floatingPassword">Password</label>
              </div>
              <div class="form-floating mb-3">
                <input name="telp" type="number" class="form-control" id="floatingInput" placeholder="telp
                ">
                <label for="floatingInput">Telepon</label>
              </div>
              <div class="d-grid">
                <input value="register" class="btn btn-primary btn-login text-uppercase fw-bold" type="submit">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>