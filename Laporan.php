<!DOCTYPE html>
<html>
<head>
	<title>Laporan</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<div class="container-fluid">
    		<a class="navbar-brand" href="Dashboard.php">Pengaduan Masyarakat</a>
    			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      				<span class="navbar-toggler-icon"></span>
    			</button>
    		<div class="collapse navbar-collapse" id="navbarSupportedContent">
      			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        			<li class="nav-item">
          				<a class="nav-link active" aria-current="page" href="Pengaduan.php">Pengaduan</a>
        			</li>
        			<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Tanggapan.php">Tanggapan</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Konfirmasi.php">Konfirmasi</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Laporan.php">Laporan</a>
		        	</li>
		      	</ul>
		      		<form class="d-flex">
		        		<a href="Login.php" class="btn btn-outline-danger" type="submit">Log Out</a>
		      		</form>
		    </div>
		</div>
	</nav>

	<div class="container mt-5 bg-light">
		<table class="table table-striped">
		  	<thead>
		    	<tr>
		      		<th scope="col">Nik</th>
		      		<th scope="col">Nama</th>
		      		<th scope="col">No Telepon</th>
		      		<th scope="col">Pengaduan</th>
		      		<th scope="col">Tanggal</th>
		    	</tr>
		  	</thead>
		  	<tbody>
		    	<tr>
		      		<td>131313</td>
		      		<td>Sukma</td>
		      		<td>0869420</td>
		      		<td>Rumah saya dijarah Pack Renaldi</td>
		      		<td>30 Februari 2077</td>
		    	</tr>

		      		<td>121212</td>
		      		<td>Renaldi</td>
		      		<td>0888888</td>
		      		<td>Saya diusir Pack Sukma</td>
		      		<td>30 Februari 2077</td>
		    	</tr>
		  	</tbody>
		</table>
	</div>
 	
 	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>