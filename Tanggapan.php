<!DOCTYPE html>
<html>
<head>
	<title>Tanggapan</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<div class="container-fluid">
    		<a class="navbar-brand" href="Dashboard.php">Pengaduan Masyarakat</a>
    			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      				<span class="navbar-toggler-icon"></span>
    			</button>
    		<div class="collapse navbar-collapse" id="navbarSupportedContent">
      			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        			<li class="nav-item">
          				<a class="nav-link active" aria-current="page" href="Pengaduan.php">Pengaduan</a>
        			</li>
        			<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Tanggapan.php">Tanggapan</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Konfirmasi.php">Konfirmasi</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Laporan.php">Laporan</a>
		        	</li>
		      	</ul>
		      		<form class="d-flex">
		        		<a href="Login.php" class="btn btn-outline-danger" type="submit">Log Out</a>
		      		</form>
		    </div>
		</div>
	</nav>

	<div class="container tanggapan">
		<div class="accordion accordion-flush" id="accordionFlushExample">
  			<div class="accordion-item">
    			<h2 class="accordion-header" id="flush-headingOne">
      				<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
        				Laporan Pack Sukma
      				</button>
    			</h2>
    			<div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
      				<div class="accordion-body">
      					<p>Admin#1 : Baik Pack Sukma, saya akan berbicara dengan Pack Renaldi.</p>
      				</div>
    			</div>
  			</div>
			<div class="accordion-item">
			    <h2 class="accordion-header" id="flush-headingTwo">
			      	<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
			        	Laporan Pack Renaldi 
			      	</button>
			    </h2>
			    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
			     	<div class="accordion-body">
			     		<p>Admin#1 : Pack Renaldi, tolong jangan mengganggu Pack Sukma ya. Tadi dia lapor ke saya Bapack dateng ke rumah Pack Sukma saat Pack Sukma mau uhui uhui sama Istri nya.</p>
			     	</div>
			    </div>
			</div>
	</div>
 	
 	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>