<!DOCTYPE html>
<html>
<head>
	<title>Pengaduan</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<div class="container-fluid">
    		<a class="navbar-brand" href="Dashboard.php">Pengaduan Masyarakat</a>
    			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      				<span class="navbar-toggler-icon"></span>
    			</button>
    		<div class="collapse navbar-collapse" id="navbarSupportedContent">
      			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
        			<li class="nav-item">
          				<a class="nav-link active" aria-current="page" href="Pengaduan.php">Pengaduan</a>
        			</li>
        			<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Tanggapan.php">Tanggapan</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Konfirmasi.php">Konfirmasi</a>
		        	</li>
		        	<li class="nav-item">
		          		<a class="nav-link active" aria-current="page" href="Laporan.php">Laporan</a>
		        	</li>
		      	</ul>
		      		<form class="d-flex">
		        		<a href="Login.php" class="btn btn-outline-danger" type="submit">Log Out</a>
		      		</form>
		    </div>
		</div>
	</nav>

		<div class="container pengaduan">
			<h5 class="card-title text-center mb-5 fw-light fs-5">Form Pengaduan</h5>
			<form method="POST" action="kirimPengaduan.php" enctype="multipart/form-data">
				<div class="mb-3">
				 	<label for="exampleFormControlTextarea1" class="form-label">Keluhan</label>
				  	<textarea name="aduan" class="form-control" id="exampleFormControlTextarea1" rows="5" title="Ajukan Keluh Kesah mu Di sini"></textarea>
				</div>
				<div class="mb-3">
				  	<label for="formFileMultiple" class="form-label">Gambar</label>
				  	<input name="gambar" class="form-control" type="file" id="formFile" title="Sertakan Gambar Laporan">
				</div>

				<div class="mb-3">
			  		<input value="Laporkan" class="btn btn-primary" type="submit">
			    </div>
			
				<div class="mb-3">
					<ul class="list-group" style="padding: 20px">
						<li class="list-group-item d-flex justify-content-between align-items-center">
					    	A list item
					    	<span class="badge bg-primary rounded-pill">69</span>
						</li>
					</ul>
				</div>
			</form>
		</div>
 	
 	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>